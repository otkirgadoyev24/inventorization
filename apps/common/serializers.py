from rest_framework import serializers
from apps.common.models import Section, Room
from apps.instruments.models import UserInstrument


class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = [
            "id", "name"
        ]


class RoomSerializer(serializers.ModelSerializer):
    # section = serializers.SerializerMethodField()

    class Meta:
        model = Room
        fields = ["id", "name", "section"]

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["section"] = SectionSerializer(instance.section).data
        return representation



class InstrumentAttachedSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInstrument
        fields = (
            "id",
            "instrument",
            "user",
            "inventory_number"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["instrument"] = instance.instrument.description
        representation["inventory"] = instance.instrument.inventory_number
        representation["user"] = f"{instance.user.get_full_name()}"
        return representation


class UserInstrumentExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInstrument
        fields = (
            "user",
            "instrument",
            "inventory_number"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["full_name"] = instance.user.get_full_name()
        representation["section"] = instance.user.section.name
        representation["room"] = RoomSerializer(instance.user.room, many=True).data
        representation["inventory"] = instance.instrument.inventory_number
        representation["category"] = instance.instrument.category.name
        representation["description"] = instance.instrument.description
        return representation


class UserInstrumentCardExportSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format="%d.%m.%Y")
    class Meta:
        model = UserInstrument
        fields = (
            "user",
            "instrument",
            "inventory_number",
            "created_at"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["full_name"] = instance.user.get_full_name()
        representation["section"] = instance.user.section.name
        representation["room"] = RoomSerializer(instance.user.room, many=True).data
        representation["price"] = f"{instance.price} сўм"
        representation["description"] = instance.instrument.description
        return representation
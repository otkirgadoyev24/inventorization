from django.contrib import admin
from apps.common.models import Room, Section


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    list_display = ["id", "name"]


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "section"]

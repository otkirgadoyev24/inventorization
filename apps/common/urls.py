from django.urls import path
from apps.common import views

urlpatterns = [
    path("section/", views.SectionView.as_view(), name="section"),
    path("section/<int:pk>/", views.SectionDetailView.as_view(), name="section_detail"),
    path("room/", views.RoomView.as_view(), name="room"),
    path("room/<int:pk>/", views.RoomDetailView.as_view(), name="room_detail"),
    path("instrument_export/", views.InstrumentAttachedExportView.as_view(), name="export"),
    path("user_instrument_export/", views.UserInstrumentExportView.as_view(), name="user_instrument_export"),
    path("user_instrument_card_export/", views.UserInstrumentCardExportView.as_view(), name="user_instrument_card_export")
]

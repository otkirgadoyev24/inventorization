from django.core.validators import MinValueValidator
from django.db.models import BooleanField, CharField, IntegerField
from django.utils.translation import gettext_lazy as _


# Bu klassni tartiblash uchun ishlatamiz
class OrderField(IntegerField):
    def __init__(
            self, verbose_name=_("tartibi"), default=1, validators=None, **kwargs
    ):
        if validators is None:
            validators = [MinValueValidator(1)]
        super().__init__(
            verbose_name=verbose_name,
            default=default,
            validators=validators,
            **kwargs
        )


# Bu klassni activeligini bildirish uchun ishlatamiz
class ActiveField(BooleanField):
    def __init__(self, verbose_name=_("aktiv"), default=True, **kwargs):
        super().__init__(verbose_name=verbose_name, default=default, **kwargs)

from django.db import models
from django.utils.translation import gettext_lazy as _
from .base import BaseModel


class Section(BaseModel):
    name = models.CharField(_("Nomi"), max_length=255, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Bo'lim"
        verbose_name_plural = "Bo'limlar"


class Room(BaseModel):
    name = models.CharField(_("Nomi"), max_length=255, null=True)
    section = models.ForeignKey(Section, on_delete=models.SET_NULL, null=True, related_name="rooms", verbose_name=_("Bo'lim"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Xona"
        verbose_name_plural = "Xonalar"

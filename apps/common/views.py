from datetime import datetime
from docxtpl import DocxTemplate
from rest_framework.response import Response
from django.http import Http404, HttpResponse
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from apps.common.models import Section, Room
from apps.common.serializers import (
    SectionSerializer,
    RoomSerializer,
    InstrumentAttachedSerializer,
    UserInstrumentExportSerializer,
    UserInstrumentCardExportSerializer,
)
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from rest_framework.views import APIView
from apps.instruments.models import UserInstrument


class SectionView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Section.objects.all().order_by("-created_at")
    serializer_class = SectionSerializer


class SectionDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Section.objects.all()
    serializer_class = SectionSerializer

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class RoomView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Room.objects.select_related("section").order_by("-created_at")
    serializer_class = RoomSerializer

    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = [
        "section"
    ]
    search_fields = ["name"]


class RoomDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Room.objects.select_related("section")
    serializer_class = RoomSerializer

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class InstrumentAttachedExportView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        pk = self.request.query_params.get("pk")
        try:
            user_instrument = UserInstrument.objects.filter(instrument_id=pk).select_related(
                "user",
                "instrument",
                "user__section",
                "instrument__category"
            ).prefetch_related(
                "user__room",
                "instrument__document"
            )
        except UserInstrument.DoesNotExist:
            raise Http404
        serializer = InstrumentAttachedSerializer(user_instrument, many=True).data
        file_name = f"TMB buyicha_{datetime.now().date()}.docx"
        temp_file_path = f"media/report/{file_name}"
        word_template_path = "media/TMB.docx"
        document = DocxTemplate(word_template_path)
        datas = []
        for i in range(len(serializer)):
            datas.append(
                {
                    "ID": i + 1,
                    "DATE": datetime.now().date(),
                    "INSTRUMENT_INVENTORY_NUMBER": serializer[0]["inventory"],
                    "DESCRIPTION": serializer[0]["instrument"],
                    "USER": serializer[i]["user"],
                    "INVENTORY_NUMBER": serializer[i]["inventory_number"],
                }
            )
        context = {"results": datas}
        document.render(context)
        document.save(temp_file_path)
        response = HttpResponse(open(temp_file_path, 'rb'))
        full_url = f'{request.scheme}://{request.get_host()}/{temp_file_path}'
        response['Content-Disposition'] = f'attachment; filename="{file_name}"'

        return Response({"file_url": full_url})


class UserInstrumentExportView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        pk = self.request.query_params.get("pk")
        try:
            user_instrument = UserInstrument.objects.filter(user_id=pk).select_related(
                "user",
                "instrument",
                "user__section",
                "instrument__category"
            ).prefetch_related(
                "user__room",
                "instrument__document"
            )
        except UserInstrument.DoesNotExist:
            raise Http404
        serializer = UserInstrumentExportSerializer(user_instrument, many=True).data
        file_name = f"Ходимга бириктирилган TMBлар buyicha_{datetime.now().date()}.docx"
        temp_file_path = f"media/report/{file_name}"
        word_template_path = "media/Ходимга бириктирилган TMBлар buyicha.docx"
        document = DocxTemplate(word_template_path)
        datas = []
        for i in range(len(serializer)):
            datas.append(
                {
                    "ID": i + 1,
                    "DATE": datetime.now().date(),
                    "INSTRUMENT_INVENTORY_NUMBER": serializer[0]["inventory"],
                    "FULL_NAME": serializer[0]["full_name"],
                    "SECTION": serializer[0]["section"],
                    "ROOM": serializer[0]["room"][0]["name"],
                    "NUM": serializer[i]["inventory_number"],
                    "CAT": serializer[i]["category"],
                    "DESCRIPTION": serializer[i]["description"],
                }
            )
        context = {"results": datas}
        document.render(context)
        document.save(temp_file_path)
        response = HttpResponse(open(temp_file_path, 'rb'))
        full_url = f'{request.scheme}://{request.get_host()}/{temp_file_path}'
        response['Content-Disposition'] = f'attachment; filename="{file_name}"'

        return Response({"file_url": full_url})


class UserInstrumentCardExportView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        pk = self.request.query_params.get("pk")
        try:
            user_instrument = UserInstrument.objects.filter(user_id=pk).select_related(
                "user",
                "instrument",
                "user__section",
                "instrument__category"
            ).prefetch_related(
                "user__room",
                "instrument__document"
            )
        except UserInstrument.DoesNotExist:
            raise Http404
        serializer = UserInstrumentCardExportSerializer(user_instrument, many=True).data
        file_name = f"ТМБ карточка бўйича_{datetime.now().date()}.docx"
        temp_file_path = f"media/report/{file_name}"
        word_template_path = "media/ТМБ карточка бўйича.docx"
        document = DocxTemplate(word_template_path)
        datas = []
        for i in range(len(serializer)):
            datas.append(
                {
                    "ID": i + 1,
                    "DATE": datetime.now().date(),
                    "PRICE": serializer[i]["price"],
                    "FULL_NAME": serializer[i]["full_name"],
                    "SECTION": serializer[i]["section"],
                    "NUM": serializer[i]["inventory_number"],
                    "DESCRIPTION": serializer[i]["description"],
                    "C": serializer[i]["created_at"]
                }
            )
        context = {"results": datas}
        document.render(context)
        document.save(temp_file_path)
        response = HttpResponse(open(temp_file_path, 'rb'))
        full_url = f'{request.scheme}://{request.get_host()}/{temp_file_path}'
        response['Content-Disposition'] = f'attachment; filename="{file_name}"'

        return Response({"file_url": full_url})

from rest_framework import serializers
from apps.instruments.models import Category, Document, Instrument, UserInstrument, OutputInstrument, Store, Lost
from django.db.models import Q, Count, Sum
from apps.users.serializers import UserDetailSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            "id",
            "name"
        )


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = (
            "id",
            "name",
            "number",
            "type",
            "form",
            "file",
            "signed_at"
        )


class DocumentForAttachSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format="%Y-%m-%d", required=False)

    class Meta:
        model = Document
        fields = (
            "id",
            "name",
            "number",
            "type",
            "form",
            "file",
            "signed_at",
            "created_at"
        )


class InstrumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instrument
        fields = (
            "id",
            "name",
            "category",
            "avatar",
            "inventory_number",
            "amount",
            "total_price",
            "description",
            "manufactured_date",
            "document",
            "measurement_type",
            # "store_count",
            # "lost_count"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["category"] = CategorySerializer(instance.category).data
        representation['document'] = []
        for entry in instance.document.all():
            document = DocumentSerializer(entry).data
            representation['document'].append(document)
        return representation


class InstrumentAttachSerializer(serializers.ModelSerializer):
    count = serializers.SerializerMethodField()

    class Meta:
        model = Instrument
        fields = (
            "id",
            "name",
            "category",
            "inventory_number",
            "total_price",
            "count"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["category"] = CategorySerializer(instance.category).data
        return representation

    def get_count(self, obj):
        queryset = (
            Instrument.objects.aggregate(
                all=Sum("amount", filter=Q(id=obj.id))
            ),
            UserInstrument.objects.aggregate(
                attached=Count("id", distinct=True,
                               filter=Q(instrument=obj))),
            OutputInstrument.objects.aggregate(
                outputs=Sum("count", distinct=True,
                            filter=Q(instrument=obj))
            ),
            Store.objects.aggregate(
                store_count=Sum("count", distinct=True,
                                filter=Q(instrument=obj))
            ),
            Lost.objects.aggregate(
                lost_count=Sum("count", distinct=True,
                               filter=Q(instrument=obj))
            ))
        return queryset


class UserInstrumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInstrument
        fields = (
            "id",
            "user",
            "instrument",
            "count",
            "price",
            "inventory_number"
        )
        extra_kwargs = {
            "price": {"read_only": True},
            "inventory_number": {"read_only": True}
        }

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["instrument"] = (InstrumentSerializer(instance.instrument)).data
        representation["user"] = (UserDetailSerializer(instance.user)).data
        return representation


class OutputInstrumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = OutputInstrument
        fields = (
            "id",
            # "user",
            "instrument",
            "user_instrument",
            "document",
            "count",
            "description"
        )

        extra_kwargs = {
            "user_instrument": {"required": False},
            "document": {"required": True},
            "count": {"required": True},
        }

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["instrument"] = (InstrumentSerializer(instance.instrument)).data
        representation["user"] = (UserDetailSerializer(instance.user)).data
        representation["document"] = (DocumentSerializer(instance.document)).data
        representation["user_instrument"] = (UserInstrumentSerializer(instance.user_instrument)).data
        return representation

    def validate(self, attrs):
        count = attrs.get("count")
        instrument = attrs.get("instrument")
        instrument_amount = Instrument.objects.get(id=instrument.id)
        if count > instrument_amount.amount:
            raise serializers.ValidationError("Hisobdan chiqarilganlar soni jami miqdordan katta bo'lolmaydi.")
        return attrs


class OutputInstrumentForAttachSerializer(serializers.ModelSerializer):
    class Meta:
        model = OutputInstrument
        fields = (
            "id",
            "document",
            "count",
            "description"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["document"] = (DocumentForAttachSerializer(instance.document)).data
        return representation


class InstrumentAttachDetailSerializer(serializers.ModelSerializer):
    count = serializers.SerializerMethodField()
    output = serializers.SerializerMethodField()
    user_instrument = serializers.SerializerMethodField()

    class Meta:
        model = Instrument
        fields = (
            "id",
            "name",
            "category",
            "inventory_number",
            "count",
            "output",
            "user_instrument",
            "description",
            "avatar"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["category"] = CategorySerializer(instance.category).data
        return representation

    def get_count(self, obj):
        queryset = (
            Instrument.objects.aggregate(
                all=Sum("amount", filter=Q(id=obj.id))
            ),
            UserInstrument.objects.aggregate(
                attached=Count("id", distinct=True,
                               filter=Q(instrument=obj))),
            OutputInstrument.objects.aggregate(
                outputs=Sum("count", distinct=True,
                            filter=Q(instrument=obj))
            )
        )
        return queryset

    def get_output(self, obj):
        outputs = OutputInstrument.objects.filter(instrument=obj).select_related("instrument").select_related(
            "user").select_related("instrument__category").prefetch_related("instrument__document").prefetch_related(
            "user__room").select_related("user__section").prefetch_related("user__room__section").select_related(
            "user_instrument").select_related("user_instrument__instrument").select_related(
            "user_instrument__user").select_related("user_instrument__instrument__category").prefetch_related(
            "user_instrument__instrument__document").prefetch_related("user_instrument__user__room").select_related(
            "user_instrument__user__section").prefetch_related("user_instrument__user__room__section").select_related(
            "document")
        serializer = OutputInstrumentForAttachSerializer(outputs, many=True).data
        return serializer

    def get_user_instrument(self, obj):
        user_instruments = UserInstrument.objects.filter(instrument=obj).select_related("instrument").select_related(
            "user").select_related("instrument__category").prefetch_related("instrument__document").prefetch_related(
            "user__room").select_related("user__section").prefetch_related("user__room__section")
        serializer = UserInstrumentSerializer(user_instruments, many=True)
        return serializer.data


class UserInstrumentOutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = OutputInstrument
        fields = (
            "id",
            "user_instrument",
            "document",
            "description"
        )
        extra_kwargs = {
            "document": {"required": True},
            "user_instrument": {"required": True},
        }

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["instrument"] = (InstrumentSerializer(instance.instrument)).data
        representation["document"] = (DocumentSerializer(instance.document)).data
        representation["user_instrument"] = (UserInstrumentSerializer(instance.user_instrument)).data
        return representation


class UserRoleInstrumentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInstrument
        fields = (
            "id",
            "user",
            "instrument",
            "count",
            "price",
            "inventory_number"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["instrument"] = (InstrumentSerializer(instance.instrument)).data
        representation["user"] = (UserDetailSerializer(instance.user)).data
        return representation


class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = (
            "id",
            "instrument",
            "count"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["instrument"] = InstrumentSerializer(instance.instrument).data
        return representation


class LostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = (
            "id",
            "instrument",
            "count"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["instrument"] = InstrumentSerializer(instance.instrument).data
        return representation

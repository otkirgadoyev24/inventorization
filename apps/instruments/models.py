from django.db import models
from django.utils.translation import gettext_lazy as _
from apps.common.models import BaseModel, Room, Section
from sorl.thumbnail import ImageField
from apps.instruments.choices import DocumentTypes, DocumentForm, MeasurementType
from django.utils.html import mark_safe

from apps.users.models import User


class Category(BaseModel):
    name = models.CharField(_("Nomi"), max_length=255, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Kategoriya")
        verbose_name_plural = _("Kategoriyalar")


class Document(BaseModel):
    name = models.CharField(_("Nomi"), max_length=255, null=True)
    number = models.CharField(_("Raqami"), max_length=255, null=True)
    file = models.FileField(_("Fayl"), upload_to="documents/", null=True, blank=True)
    type = models.CharField(_("Hujjat turi"), max_length=255, choices=DocumentTypes.choices, null=True, blank=True)
    form = models.CharField("Hujjat shakli", max_length=255, choices=DocumentForm.choices, null=True)
    signed_at = models.DateField(_("Imzolangan sana"), null=True)

    def __str__(self):
        return self.name if self.name else ""

    class Meta:
        verbose_name = _("Hujjat")
        verbose_name_plural = _("Hujjatlar")

    # def save(self, *args, **kwargs):
    #     self.name = os.path.basename(self.file.path)
    #     super().save(*args, **kwargs)


class Instrument(BaseModel):
    name = models.CharField(_("Nomi"), max_length=255, null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="instruments",
                                 verbose_name=_("Kategoriya"), null=True)
    avatar = ImageField(upload_to="instruments/avatars/", verbose_name=_("Avatar"), null=True, blank=True)
    inventory_number = models.CharField(_("Inventar raqam"), max_length=31, null=True)
    amount = models.PositiveIntegerField(_("Miqdori"), null=True)
    total_price = models.PositiveBigIntegerField(_("Umumiy narxi"), null=True)
    description = models.TextField(_("Xarakteristikasi"), null=True)
    manufactured_date = models.DateField(_("Ishlab chiqarilgan sana"), null=True)
    document = models.ManyToManyField(Document, related_name="instruments", verbose_name=_("Hujjat"))
    measurement_type = models.CharField(_("O'lchov turi"), max_length=255, choices=MeasurementType.choices, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Tovar")
        verbose_name_plural = _("Tovarlar")

    def image_tag(self):
        return mark_safe('<img src="%s" width="70px" height="70px" />' % self.avatar.url) if self.avatar else ""

    image_tag.short_description = 'Rasmi'


class UserInstrument(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user_instruments",
                             verbose_name=_("Xodim"))
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, related_name="user_instruments",
                                   verbose_name=_("Tovar"))
    count = models.PositiveIntegerField(_("Biriktirilgan tovarlar soni"), null=True)
    price = models.PositiveIntegerField(_("Tovar narxi"), null=True)
    inventory_number = models.CharField(_("Inventar raqam"), max_length=31, null=True)

    def __str__(self):
        return self.inventory_number if self.inventory_number else ""

    class Meta:
        verbose_name = _("Biriktirilgan tovar")
        verbose_name_plural = _("Biriktirilgan tovarlar")


class OutputInstrument(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="output_instruments",
                             verbose_name=_("Xodim"), null=True, blank=True)
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, related_name="output_instruments",
                                   verbose_name=_("Tovar"), null=True)
    user_instrument = models.ForeignKey(UserInstrument, on_delete=models.CASCADE, related_name="output_instruments",
                                        verbose_name=_("Biriltirilgan tovar"), null=True, blank=True)
    document = models.ForeignKey(Document, on_delete=models.SET_NULL, related_name="output_instruments", null=True,
                                 verbose_name=_("Hujjat"))
    count = models.PositiveIntegerField(_("Hisobdan chiqarilgan tovarlar soni"), null=True)
    description = models.TextField(_("Hisobdan chiqarish bo'yicha izoh"), null=True, blank=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Hisobdan chiqarilgan tovar")
        verbose_name_plural = _("Hisobdan chiqarilgan tovarlar")

    def save(self, *args, **kwargs):
        self.instrument.total_price = float(self.instrument.total_price) - float(
            self.instrument.total_price / self.instrument.amount) * float(self.count)
        self.instrument.amount = float(self.instrument.amount) - float(self.count)
        self.instrument.save()
        super().save(*args, **kwargs)


# 521400


class Store(BaseModel):
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, related_name="store",
                                   verbose_name=_("Tovar"), null=True)
    count = models.PositiveIntegerField(_("Omborxonada qolgan tovarlar soni"), null=True, blank=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Omborxonada qolgan tovarlar")
        verbose_name_plural = _("Omborxonada qolgan tovarlar")


class Lost(BaseModel):
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, related_name="lost",
                                   verbose_name=_("Tovar"), null=True)
    count = models.PositiveIntegerField(_("Kamomat"), null=True, blank=True)
    is_lost = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Kamomat")
        verbose_name_plural = _("Kamomat")

# Generated by Django 5.0 on 2023-12-12 04:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('instruments', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='kiritilgan sana')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name="o'zgartirilgan sana")),
                ('name', models.CharField(max_length=255, null=True, verbose_name='Nomi')),
                ('number', models.CharField(max_length=255, null=True, verbose_name='Raqami')),
                ('file', models.FileField(blank=True, null=True, upload_to='documents/', verbose_name='Fayl')),
                ('type', models.CharField(blank=True, choices=[('order', 'Order'), ('decision', 'Decision'), ('decree', 'Decree')], max_length=255, null=True, verbose_name='Hujjat turi')),
                ('form', models.CharField(max_length=255, null=True, verbose_name='Hujjat shakli')),
            ],
            options={
                'verbose_name': 'Hujjat',
                'verbose_name_plural': 'Hujjatlar',
            },
        ),
    ]

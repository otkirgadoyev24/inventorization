from django.db import models
from django.utils.translation import gettext_lazy as _


class DocumentTypes(models.TextChoices):
    ORDER = "order", _("Order")  # Buyruq
    DECISION = "decision", _("Decision")  # qaror
    DECREE = "decree", _("Decree")  # farmoyish


class DocumentForm(models.TextChoices):
    INCOME = "income", _("Income")  # Kirim
    OUTPUT = "output", _("Output")  # Chiqim


class MeasurementType(models.TextChoices):
    PIECE = "piece", _("Piece")  # Dona
    SET = "set", _("Set")  # Komplekt

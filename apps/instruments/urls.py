from django.urls import path

from apps.instruments import views

urlpatterns = [
    path("category/", views.CategoryView.as_view(), name="category"),
    path("category/<int:pk>/", views.CategoryDetailView.as_view(), name="category_detail"),
    path("document/", views.DocumentView.as_view(), name="document"),
    path("document/<int:pk>/", views.DocumentDetailView.as_view(), name="document_detail"),
    path("", views.InstrumentView.as_view(), name="instrument"),
    path("<int:pk>/", views.InstrumentDetailView.as_view(), name="instrument_detail"),
    path("attach/list/", views.InstrumentAttachView.as_view(), name="instrument_attach"),
    path("attach/<int:pk>/", views.InstrumentAttachDetailView.as_view(), name="instrument_attach_detail"),
    path("user_instrument/", views.UserInstrumentView.as_view(), name="user_instrument"),
    path("user_instrument/<int:pk>/", views.UserInstrumentDetailView.as_view(), name="user_instrument_detail"),
    path("output_instrument/", views.OutputInstrumentView.as_view(), name="output_instrument"),
    path("user_instrument_output/", views.UserInstrumentOutputView.as_view(), name="user_instrument_output"),
    path("user_role_instruments/", views.UserRoleInstrumentsView.as_view(), name="user_role_instruments"),
    path("user_role_instruments/<int:pk>/", views.UserRoleInstrumentDetailView.as_view(),
         name="user_role_instruments_detail"),
    path("store/", views.StoreCreateView.as_view(), name="store"),
    path("store/list/", views.StoreListView.as_view(), name="store"),
    path("store/<int:pk>/", views.StoreDetailView.as_view(), name="store"),
    path("lost/list/", views.LostListView.as_view(), name="lost"),
    path("lost/", views.LostCreateView.as_view(), name="lost"),
    path("lost/<int:pk>/", views.LostDetailView.as_view(), name="lost"),
]

from django.contrib import admin
from apps.instruments.models import Category, Document, Instrument, UserInstrument, OutputInstrument, Store, Lost
from django.contrib.auth.models import Group

admin.site.unregister(Group)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ["id", "name"]


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "number", "type", "form", "file", "signed_at"]


@admin.register(Instrument)
class InstrumentAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "category", "inventory_number", "measurement_type", "avatar", "image_tag", "amount",
                    "total_price"]
    list_display_links = ["id", "name"]
    list_filter = ["category", "measurement_type", "document"]


@admin.register(UserInstrument)
class UserInstrumentAdmin(admin.ModelAdmin):
    list_display = ["id", "user", "instrument", "inventory_number", "count", "price"]


@admin.register(OutputInstrument)
class OutputInstrumentAdmin(admin.ModelAdmin):
    list_display = ["id", "instrument", "user_instrument", "document", "count"]


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    list_display = ["id", "instrument", "count"]


@admin.register(Lost)
class LostAdmin(admin.ModelAdmin):
    list_display = ["id", "instrument", "count"]

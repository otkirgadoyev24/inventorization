from apps.instruments.serializers import (
    CategorySerializer,
    DocumentSerializer,
    InstrumentSerializer,
    InstrumentAttachSerializer,
    UserInstrumentSerializer,
    OutputInstrumentSerializer,
    InstrumentAttachDetailSerializer,
    UserInstrumentOutputSerializer,
    UserRoleInstrumentsSerializer,
    StoreSerializer,
    LostSerializer
)
from apps.instruments.models import (
    Category,
    Document,
    Instrument,
    UserInstrument,
    OutputInstrument,
    Store,
    Lost
)
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from django.db.models import Q
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response

from apps.users.choices import Roles


class CategoryView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Category.objects.all().order_by('-created_at')
    serializer_class = CategorySerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]

    def get_queryset(self):
        search = self.request.query_params.get("search", None)
        if search:
            return self.queryset.filter(
                Q(name__icontains=search) | Q(name__icontains=search.lower()) | Q(name__icontains=search.upper()) | Q(
                    name__icontains=search.capitalize()) | Q(name__icontains=search.title()))
        else:
            return self.queryset


class CategoryDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class DocumentView(generics.ListCreateAPIView):
    parser_classes = [FormParser, MultiPartParser]
    permission_classes = [IsAuthenticated]
    queryset = Document.objects.all().order_by("-created_at")
    serializer_class = DocumentSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["type", "form", "signed_at"]

    def create(self, request, *args, **kwargs):
        number = request.data.get("number", None)
        name = request.data.get("name", None)
        _type = request.data.get("type", None)
        form = request.data.get("form", None)
        signed_at = request.data.get("signed_at", None)
        file = request.data.get("file[]", None)
        doc = Document.objects.create(file=file, type=_type, form=form, number=number, name=name, signed_at=signed_at)
        return Response(DocumentSerializer(doc).data)

    def get_queryset(self):
        search = self.request.query_params.get("search", None)
        if search:
            return self.queryset.filter(
                Q(name__icontains=search) | Q(name__icontains=search.lower()) | Q(name__icontains=search.upper()) | Q(
                    name__icontains=search.capitalize()) | Q(name__icontains=search.title()) |
                Q(number__icontains=search)
            )
        else:
            return self.queryset


class DocumentDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    parser_classes = [FormParser, MultiPartParser]
    queryset = Document.objects.all().order_by("-created_at")
    serializer_class = DocumentSerializer

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class InstrumentView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    parser_classes = [FormParser, MultiPartParser]
    queryset = (Instrument.objects.
                select_related("category").
                prefetch_related("document").
                order_by("-created_at"))
    serializer_class = InstrumentSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["category", "measurement_type", "manufactured_date"]

    # search_fields = ["inventory_number"]  # name, document__number katta kichik

    def get_queryset(self):
        search = self.request.query_params.get("search", None)
        if search:
            return self.queryset.filter(
                Q(name__icontains=search) | Q(name__icontains=search.lower()) | Q(name__icontains=search.upper()) | Q(
                    name__icontains=search.capitalize()) | Q(
                    name__icontains=search.title()) | Q(document__number__icontains=search) | Q(
                    inventory_number__icontains=search))
        else:
            return self.queryset


class InstrumentDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    parser_classes = [FormParser, MultiPartParser]
    queryset = Instrument.objects.select_related("category").prefetch_related("document").order_by("-created_at")
    serializer_class = InstrumentSerializer

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class InstrumentAttachView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    parser_classes = [FormParser, MultiPartParser]
    queryset = (Instrument.objects.
                select_related("category").
                prefetch_related("document").
                order_by("-created_at"))
    serializer_class = InstrumentAttachSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["category"]

    def get_queryset(self):
        search = self.request.query_params.get("search", None)
        if search:
            return self.queryset.filter(
                Q(name__icontains=search) | Q(name__icontains=search.lower()) | Q(name__icontains=search.upper()) | Q(
                    name__icontains=search.capitalize()) | Q(
                    name__icontains=search.title()) | Q(document__number__icontains=search) | Q(
                    inventory_number__icontains=search))
        else:
            return self.queryset


class UserInstrumentView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = (UserInstrument.objects.
                select_related("instrument").
                select_related("user").
                select_related("instrument__category").
                prefetch_related("instrument__document").
                prefetch_related("user__room").
                select_related("user__section").
                prefetch_related("user__room__section")
                ).order_by("-created_at")
    serializer_class = UserInstrumentSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["instrument__category", "user__section"]

    def create(self, request, *args, **kwargs):
        data = request.data
        count = data.get('count', 1)
        user = data["user"]
        instrument = data["instrument"]
        my_instrument = Instrument.objects.get(id=instrument)
        price = (my_instrument.total_price / my_instrument.amount)
        inventors_count = UserInstrument.objects.filter(instrument=my_instrument).count()
        instances = [
            UserInstrument(user_id=user, instrument_id=instrument, count=1, price=price,
                           inventory_number=f"{my_instrument.inventory_number}/{inventors_count + (_ + 1)}")
            for _ in range(count)
        ]
        UserInstrument.objects.bulk_create(instances)
        try:
            store = Store.objects.get(instrument=instrument)
            store.count -= count
            store.save()
        except Exception as e:
            print("error", e)

        serializer = self.get_serializer(instances, many=True)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        search = self.request.query_params.get("search", None)
        if search:
            return self.queryset.filter(
                Q(instrument__name__icontains=search) | Q(instrument__name__icontains=search.lower()) | Q(
                    instrument__name__icontains=search.upper()) | Q(
                    instrument__name__icontains=search.capitalize()) | Q(
                    instrument__name__icontains=search.title()) | Q(
                    inventory_number__icontains=search) |
                Q(user__last_name__icontains=search) | Q(user__last_name__icontains=search.lower()) | Q(
                    user__last_name__icontains=search.upper()) | Q(
                    user__last_name__icontains=search.capitalize()) | Q(
                    user__last_name__icontains=search.title()))
        else:
            return self.queryset


class UserInstrumentDetailView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    queryset = (UserInstrument.objects.
                select_related("instrument").
                select_related("user").
                select_related("instrument__category").
                prefetch_related("instrument__document").
                prefetch_related("user__room").
                select_related("user__section").
                prefetch_related("user__room__section").
                order_by("-created_at"))
    serializer_class = UserInstrumentSerializer


class OutputInstrumentView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    parser_classes = [FormParser, MultiPartParser]
    queryset = (OutputInstrument.objects.
                select_related("instrument").
                select_related("user").
                select_related("instrument__category").
                prefetch_related("instrument__document").
                prefetch_related("user__room").
                select_related("user__section").
                prefetch_related("user__room__section").
                select_related("user_instrument").
                select_related("user_instrument__instrument").
                select_related("user_instrument__user").
                select_related("user_instrument__instrument__category").
                prefetch_related("user_instrument__instrument__document").
                prefetch_related("user_instrument__user__room").
                select_related("user_instrument__user__section").
                prefetch_related("user_instrument__user__room__section").
                select_related("document").
                order_by("-created_at"))
    serializer_class = OutputInstrumentSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["instrument__category", "document"]

    def get_queryset(self):
        search = self.request.query_params.get("search", None)
        if search:
            return self.queryset.filter(
                Q(instrument__name__icontains=search) | Q(instrument__name__icontains=search.lower()) | Q(
                    instrument__name__icontains=search.upper()) | Q(
                    instrument__name__icontains=search.capitalize()) | Q(
                    instrument__name__icontains=search.title()) | Q(
                    instrument__inventory_number__icontains=search))
        else:
            return self.queryset


class InstrumentAttachDetailView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    queryset = (Instrument.objects.
                select_related("category").
                prefetch_related("document").
                order_by("-created_at"))
    serializer_class = InstrumentAttachDetailSerializer


class UserInstrumentOutputView(generics.CreateAPIView):
    permission_classes = [IsAuthenticated]
    parser_classes = [FormParser, MultiPartParser]
    queryset = (OutputInstrument.objects.
                select_related("instrument").
                select_related("user").
                select_related("instrument__category").
                prefetch_related("instrument__document").
                prefetch_related("user__room").
                select_related("user__section").
                prefetch_related("user__room__section").
                select_related("user_instrument").
                select_related("user_instrument__instrument").
                select_related("user_instrument__user").
                select_related("user_instrument__instrument__category").
                prefetch_related("user_instrument__instrument__document").
                prefetch_related("user_instrument__user__room").
                select_related("user_instrument__user__section").
                prefetch_related("user_instrument__user__room__section").
                select_related("document"))
    serializer_class = UserInstrumentOutputSerializer

    def create(self, request, *args, **kwargs):
        user_instrument = request.data["user_instrument"]
        user_ins = UserInstrument.objects.get(id=user_instrument)
        document = request.data["document"]
        description = request.data.get("description", None)
        my_obj = OutputInstrument.objects.create(user_instrument_id=user_instrument, document_id=document, count=1,
                                                 description=description, instrument=user_ins.instrument)
        serializer = self.get_serializer(my_obj)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class UserRoleInstrumentsView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = UserInstrument.objects.select_related("instrument").select_related("user").select_related(
        "instrument__category").prefetch_related("instrument__document").prefetch_related("user__room").select_related(
        "user__section").prefetch_related("user__room__section").order_by("-created_at")
    serializer_class = UserRoleInstrumentsSerializer

    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["instrument__category", "user__last_name"]

    def get_queryset(self):
        print(self.request.user.role)
        search = self.request.query_params.get("search", None)
        if search:
            if self.request.user.role == Roles.STAFF:
                queryset = self.queryset.filter(
                    Q(instrument__name__icontains=search) | Q(instrument__name__icontains=search.lower()) | Q(
                        instrument__name__icontains=search.upper()) | Q(
                        instrument__name__icontains=search.capitalize()) | Q(
                        instrument__name__icontains=search.title()) | Q(
                        instrument__inventory_number__icontains=search), user=self.request.user)
            elif self.request.user.role == Roles.STAFF_ADMIN:
                queryset = self.queryset.filter(
                    Q(instrument__name__icontains=search) | Q(instrument__name__icontains=search.lower()) | Q(
                        instrument__name__icontains=search.upper()) | Q(
                        instrument__name__icontains=search.capitalize()) | Q(
                        instrument__name__icontains=search.title()) | Q(
                        instrument__inventory_number__icontains=search), user__section=self.request.user.section)
            else:
                queryset = self.queryset.filter(
                    Q(instrument__name__icontains=search) | Q(instrument__name__icontains=search.lower()) | Q(
                        instrument__name__icontains=search.upper()) | Q(
                        instrument__name__icontains=search.capitalize()) | Q(
                        instrument__name__icontains=search.title()) | Q(
                        instrument__inventory_number__icontains=search))
        else:
            if self.request.user.role == Roles.STAFF:
                queryset = self.queryset.filter(user=self.request.user)
            elif self.request.user.role == Roles.STAFF_ADMIN:
                queryset = self.queryset.filter(user__section=self.request.user.section)
            else:
                queryset = self.queryset
        return queryset


class UserRoleInstrumentDetailView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    queryset = UserInstrument.objects.select_related("instrument").select_related("user").select_related(
        "instrument__category").prefetch_related("instrument__document").prefetch_related("user__room").select_related(
        "user__section").prefetch_related("user__room__section").order_by("-created_at")
    serializer_class = UserRoleInstrumentsSerializer


class StoreCreateView(generics.CreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Store.objects.all()
    serializer_class = StoreSerializer


class StoreListView(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Store.objects.select_related("instrument", "instrument__category").prefetch_related(
        "instrument__document")
    serializer_class = StoreSerializer
    filterset_fields = ["instrument__category"]
    search_fields = ["instrument__inventory_number", "instrument__name"]


class StoreDetailView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Store.objects.select_related("instrument", "instrument__category").prefetch_related(
        "instrument__document")
    serializer_class = StoreSerializer


class LostCreateView(generics.CreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Lost.objects.all()
    serializer_class = LostSerializer


class LostListView(generics.ListAPIView):
    # permission_classes = [IsAuthenticated]
    queryset = Lost.objects.select_related("instrument", "instrument__category").prefetch_related(
        "instrument__document")
    serializer_class = LostSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["instrument__category"]
    search_fields = ["instrument__inventory_number", "instrument__name"]


class LostDetailView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Lost.objects.select_related("instrument", "instrument__category").prefetch_related(
        "instrument__document")
    serializer_class = LostSerializer

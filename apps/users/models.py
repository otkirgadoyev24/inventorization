from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager as AbstractUserManager
from django.db import models
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from apps.common.models import BaseModel, Room, Section
from apps.users.choices import Roles


class UserManager(AbstractUserManager):
    def _create_user(self, username, password, **extra_fields):
        if not username:
            raise ValueError("The given username must be set")

        user = self.model(username=username, **extra_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")
        return self._create_user(username, password, **extra_fields)

    def create_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(username, password, **extra_fields)


class User(AbstractUser, BaseModel):  # xodim va rahbar
    email = None
    REQUIRED_FIELDS = []  # type: ignore
    middle_name = models.CharField(_("Otasining ismi"), max_length=150, null=True, blank=True)
    phone_number = PhoneNumberField(verbose_name=_("Telefon raqam"), null=True, blank=True)
    role = models.CharField(verbose_name=_("Role"), max_length=31, choices=Roles.choices, null=True)
    room = models.ManyToManyField(Room, related_name="users", verbose_name=_("Xona"), blank=True)
    section = models.ForeignKey(Section, on_delete=models.SET_NULL, null=True, related_name="users",
                                verbose_name=_("Bo'lim"), blank=True)
    objects = UserManager()

    def __str__(self):
        return f"{self.last_name} {self.first_name} {self.middle_name}"

    class Meta:
        db_table = "xodim"
        verbose_name = _("Xodim")
        verbose_name_plural = _("Xodimlar")

    def get_full_name(self):
        full_name = "%s %s %s" % (self.last_name, self.first_name, self.middle_name)
        return full_name.strip()

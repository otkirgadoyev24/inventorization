from django.contrib.auth.password_validation import validate_password
from django.db.models import Count, Q
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from apps.common.models import Room
from apps.users.choices import Roles
from apps.users.models import User
from apps.common.serializers import SectionSerializer, RoomSerializer


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "first_name",
            "middle_name",
            "last_name",
            "phone_number",
            "role",
            "room",
            "section",
            "is_active"
        )

    # def to_representation(self, instance):
    #     representation = super().to_representation(instance)
    #     representation["section"] = SectionSerializer(instance.section).data
    #     # representation["room"] = RoomSerializer(instance.room).data
    #     return representation

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["section"] = SectionSerializer(instance.section).data
        representation['room'] = []
        for entry in instance.room.all():
            room = RoomSerializer(entry).data
            representation['room'].append(room)
        return representation


class CabinetLoginSerializer(TokenObtainPairSerializer):
    username_field = User.USERNAME_FIELD

    def validate(self, attrs):
        data = super().validate(attrs)
        data["user"] = UserDetailSerializer(self.user, context=self.context).data
        return data


class CheckUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "first_name",
            "middle_name",
            "last_name",
            "phone_number",
            "role",
            "room",
            "section"
        )

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["room"] = instance.room.name if instance.room else None
        representation["section"] = instance.section.name if instance.section else None
        return representation


class UserRegisterSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "middle_name",
            "role",
            "phone_number",
            "username",
            "password",
            "password2",
            "section",
            "room",
            "is_active"
        )
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True},
            'middle_name': {'required': True},
            'is_active': {'required': False},
            'phone_number': {'required': False},
            'room': {'required': False},
            'section': {'required': False},
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data):
        phone_number = validated_data.get('phone_number', None)
        # room = validated_data.get('room', None)
        section = validated_data.get('section', None)
        user = User.objects.create(
            username=validated_data['username'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            middle_name=validated_data['middle_name'],
            phone_number=phone_number,
            # room=room,
            section=section,
            role=validated_data['role']
        )
        room_ids = validated_data.pop("room")
        user.room.set(room_ids)

        user.set_password(validated_data['password'])
        user.save()
        return user

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["section"] = SectionSerializer(instance.section).data
        representation['room'] = []
        for entry in instance.room.all():
            room = RoomSerializer(entry).data
            representation['room'].append(room)
        return representation

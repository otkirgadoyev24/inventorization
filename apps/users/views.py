from rest_framework import generics
from rest_framework_simplejwt.views import TokenObtainPairView
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter
from django.db.models import Q
from apps.users.choices import Roles
from apps.users.models import User
from apps.users.serializers import (
    CabinetLoginSerializer,
    CheckUserSerializer,
    UserRegisterSerializer,
    UserDetailSerializer
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status


class CabinetLoginView(TokenObtainPairView):
    serializer_class = CabinetLoginSerializer


class CheckUserView(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = CheckUserSerializer

    def get_object(self):
        return self.request.user


class UserView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.select_related("section").prefetch_related("room").prefetch_related(
        "room__section").order_by("-created_at")
    serializer_class = UserRegisterSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = [
        "section",
        # "room"
    ]
    search_fields = ["first_name", "last_name", "phone_number"]

    def get_queryset(self):
        room = self.request.query_params.getlist("room", None)
        print("room", room)
        if room and room != ['']:
            return self.queryset.filter(~Q(role=Roles.SUPER_ADMIN), room__id__in=room).order_by("-created_at")
        elif room == ['']:
            return self.queryset.filter(~Q(role=Roles.SUPER_ADMIN)).order_by("-created_at")

        else:
            return self.queryset.filter(~Q(role=Roles.SUPER_ADMIN)).order_by("-created_at")


class UserDetailView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.select_related("section").prefetch_related("room")
    serializer_class = UserDetailSerializer

    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

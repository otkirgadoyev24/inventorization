from django.db import models
from django.utils.translation import gettext_lazy as _


class Roles(models.TextChoices):
    SUPER_ADMIN = "super_admin", _("Super admin")
    STAFF = "staff", _("Staff")  # Xodimlar
    STAFF_ADMIN = "staff_admin", _("Staff admin")  # Bo'lim boshlig'i
    CHIEF = "chief", _("Chief")  # Rahbarlar
